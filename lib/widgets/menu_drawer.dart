import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MenuDrawer extends StatefulWidget {
  const MenuDrawer({super.key});

  @override
  State<MenuDrawer> createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor:
          Colors.orange[180], //const Color.fromRGBO(66, 165, 245, 1.0),
      child: ListView(
        children: [
          // const DrawerHeader(
          //   decoration: BoxDecoration(
          //     color: Colors.cyanAccent,
          //   ),
          //   child: Text('welcome to baontapp'),
          // ),
          // ignore: prefer_const_constructors
          UserAccountsDrawerHeader(
            currentAccountPicture: const CircleAvatar(
                backgroundImage: AssetImage('assets/images/white.jpg')),
            accountName: const Text('baowee'),
            accountEmail: const Text('baowee@gmail.com'),
            onDetailsPressed: (() {
              Get.toNamed('/home');
              Scaffold.of(context).closeDrawer();
            }),
          ),
          ListTile(
            leading: const Icon(Icons.home),
            title: const Text('Main page'),
            onTap: () {
              Get.offNamedUntil('/home', (route) => false);
            },
          ),
          // const Divider(),
          ListTile(
            leading: const Icon(Icons.bookmark),
            title: const Text('About us'),
            onTap: () {
              Get.offNamedUntil('/about', (route) => false);
            },
          ),
          ListTile(
            leading: const Icon(Icons.baby_changing_station),
            title: const Text('Product'),
            onTap: () {
              Get.offNamedUntil('/product', (route) => false);
            },
          ),
        ],
      ),
    );
  }
}
