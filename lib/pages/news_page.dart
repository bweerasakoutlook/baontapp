import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  int totalResults = 0;
  List<dynamic> articles = [];
  String errorMessage = "";
  int page = 1;
  int pageSize = 5;
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(const Duration(milliseconds: 300));
    setState(() {
      articles.clear();
      page = 1;
    });
    getData();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted(resetFooterState: true);
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(const Duration(milliseconds: 200));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if (page < (totalResults / pageSize).ceil()) {
      if (mounted) {
        setState(() {
          page = ++page;
        });
      }
      getData();
      _refreshController.loadComplete();
    } else {
      _refreshController.resetNoData();
      _refreshController.loadNoData();
    }
  }

  Future<void> getData() async {
    var url = Uri.parse(
        'https://newsapi.org/v2/top-headlines?country=th&apiKey=8bfbddfcc1c84ddb86fa9cda43f75537&page=$page&pageSize=$pageSize');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var news = json.decode(response.body);
      setState(() {
        totalResults = news['totalResults'];
        // articles = news['articles'];
        articles.addAll(news['articles']); // concat array (list)
      });
    } else {
      setState(() {
        errorMessage = "server error please try again";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: totalResults > 0
              ? Text('total news >> $totalResults item')
              : null),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: const WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = const Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = const CircularProgressIndicator();
            } else if (mode == LoadStatus.failed) {
              body = const Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = const Text("release to load more");
            } else {
              body = const Text("No more Data");
            }
            return SizedBox(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          itemBuilder: (context, index) => Card(
              child: InkWell(
            onTap: () {
              Get.toNamed('/website', arguments: {
                'url': articles[index]['url'],
                'name': articles[index]['source']['name'],
              });
            },
            child: Column(
              children: [
                CachedNetworkImage(
                  imageUrl: '${articles[index]['urlToImage']}',
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      CircularProgressIndicator(
                          value: downloadProgress.progress),
                  errorWidget: (context, url, error) => const Icon(
                    Icons.error,
                    color: Colors.red,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    '${articles[index]['title']}',
                    style: const TextStyle(fontSize: 12),
                  ),
                )
              ],
            ),
          )),
          // itemExtent: 100.0,
          itemCount: articles.length,
        ),
      ),
    );
  }
}
