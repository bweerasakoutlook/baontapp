import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetailPage extends StatefulWidget {
  const DetailPage({super.key});

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Map<String, dynamic> product = {};
  Map<String, dynamic> detail = {};
  Future<Map<String, dynamic>>? getDataFuture;

  Future<Map<String, dynamic>> getData(int id) async {
    var response = await http
        .get(Uri.parse('https://api.codingthailand.com/api/course/$id'));
    if (response.statusCode == 200) {
      detail = json.decode(response.body);
      return detail;
    } else {
      throw Exception('server error please try again');
    }
  }

  @override
  void initState() {
    super.initState();
    product = Get.arguments;
    getDataFuture = getData(product['id']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('${product['title']}')),
      body: FutureBuilder<Map<String, dynamic>>(
        future: getDataFuture,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: ((context, index) {
                  return ListTile(
                    leading: Text('${index + 1}'),
                    title: Text('${snapshot.data!['data'][index]['ch_title']}'),
                    subtitle:
                        Text('${snapshot.data!['data'][index]['ch_dateadd']}'),
                    trailing: Chip(
                        label: Text(
                            '${snapshot.data!['data'][index]['ch_timetotal']}')),
                  );
                }),
                separatorBuilder: (context, index) => const Divider(),
                itemCount: snapshot.data!['data'].length);
          } else if (snapshot.hasError) {
            return Center(
                child: Text('${snapshot.error}',
                    style: const TextStyle(color: Colors.red)));
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
