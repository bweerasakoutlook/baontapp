import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> login(Map<dynamic, dynamic> formValue) async {
    // print((formValue['email']));
    // print(formValue['password']);
    var url = Uri.parse('https://api.codingthailand.com/api/login');
    var response = await http.post(url,
        body: {'email': formValue['email'], 'password': formValue['password']});
    if (response.statusCode == 200) {
      // print(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', response.body);
      var tokenResponse = json.decode(response.body);
      var profileUrl = Uri.parse('https://api.codingthailand.com/api/profile');
      var responesProfile = await http.get(profileUrl, headers: {
        'Authorization': 'Bearer ${tokenResponse['access_token']}'
      });
      // print(responesProfile.body);
      var profileData = json.decode(responesProfile.body);
      var user = profileData['data']['user'];
      await prefs.setString('profile', json.encode(user));
      Get.offNamedUntil('/home', (route) => false);
    } else {
      // print(response.body);
      var errorResponse = json.decode(response.body);
      Get.snackbar('process resault', errorResponse['message'],
          snackPosition: SnackPosition.BOTTOM,
          icon: const Icon(Icons.error),
          backgroundColor: Colors.blueGrey[100],
          duration: const Duration(seconds: 5),
          dismissDirection: DismissDirection.startToEnd);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.amberAccent, Colors.grey],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight)),
      child: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Image.asset('assets/images/NT-Logo-only.png', height: 80),
                const SizedBox(height: 40),
                Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 30, 15, 15),
                    child: Column(
                      children: [
                        FormBuilder(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.always,
                          child: Column(children: [
                            FormBuilderTextField(
                              name: 'email',
                              maxLines: 1,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                labelText: 'Email',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                filled: true,
                                fillColor: Colors.grey[200],
                              ),
                              validator: FormBuilderValidators.compose([
                                FormBuilderValidators.required(
                                    errorText: 'enter your email address'),
                                FormBuilderValidators.email(
                                    errorText: 'invalid email address format'),
                              ]),
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                            FormBuilderTextField(
                                name: 'password',
                                maxLines: 1,
                                keyboardType: TextInputType.visiblePassword,
                                obscureText: true,
                                decoration: InputDecoration(
                                  labelText: 'password',
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  filled: true,
                                  fillColor: Colors.grey[200],
                                ),
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(
                                      errorText: 'enter your password'),
                                  FormBuilderValidators.minLength(3,
                                      errorText:
                                          'password must least 3 character'),
                                ])),
                          ]),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: MaterialButton(
                                color: Colors.blueAccent,
                                child: const Text(
                                  'login',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  _formKey.currentState!.save();
                                  if (_formKey.currentState!.validate()) {
                                    // print((_formKey.currentState!.value));
                                    login(_formKey.currentState!.value);
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
