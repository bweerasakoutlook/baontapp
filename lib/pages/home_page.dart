import 'package:baontapp/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  // final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    // print('Home init state');
  }

  Future<void> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
    await prefs.remove('profile');
    Get.offNamedUntil('/login', (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    var or = MediaQuery.of(context).orientation;

    return Scaffold(
        drawer: const MenuDrawer(),
        appBar: AppBar(
          title: Image.asset(
            'assets/images/NT-Logo.png',
            height: 50,
          ),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () {
                logout();
              },
              icon: const Icon(Icons.logout),
              color: Colors.greenAccent,
            ),
          ],
        ),
        body: CustomScrollView(
          primary: false,
          slivers: <Widget>[
            SliverPadding(
              padding: const EdgeInsets.all(20),
              sliver: SliverGrid.count(
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: or == Orientation.portrait ? 3 : 5,
                children: <Widget>[
                  OutlinedButton(
                    style: ButtonStyle(
                        // alignment: Alignment.center,
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orange.shade100),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {
                      Get.toNamed('/about');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.home,
                          size: 60,
                        ),
                        Text(
                          'about us',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                        // alignment: Alignment.center,
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orange.shade200),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {
                      Get.toNamed('/news');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.map,
                          size: 60,
                        ),
                        Text(
                          'map',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                        // alignment: Alignment.center,
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orange.shade300),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {
                      Get.toNamed('/news');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.newspaper,
                          size: 60,
                        ),
                        Text(
                          'news',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                        // alignment: Alignment.center,
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orange.shade400),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {
                      Get.toNamed('/website');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.web,
                          size: 60,
                        ),
                        Text(
                          'website',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                        // alignment: Alignment.center,
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orange.shade500),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {
                      Get.toNamed('/about');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.camera,
                          size: 60,
                        ),
                        Text(
                          'camera',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                        // alignment: Alignment.center,
                        backgroundColor:
                            MaterialStateProperty.all(Colors.orange.shade600),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {
                      Get.toNamed('/login');
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.login,
                          size: 60,
                        ),
                        Text(
                          'login',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
