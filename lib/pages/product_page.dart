import 'package:baontapp/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProductPage extends StatefulWidget {
  const ProductPage({super.key});

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  Map<String, dynamic> products = {}; // backend format are {}
  // List<String, dynamic> covid = [];// backend format are []
  Future<Map<String, dynamic>>? getDataFuture;

  Future<Map<String, dynamic>> getData() async {
    var response =
        await http.get(Uri.parse('https://api.codingthailand.com/api/course'));
    if (response.statusCode == 200) {
      products = json.decode(response.body);
      return products;
    } else {
      throw Exception('server error please try again');
    }
  }

  @override
  void initState() {
    super.initState();
    getDataFuture = getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MenuDrawer(),
      appBar: AppBar(title: const Text('product')),
      body: FutureBuilder<Map<String, dynamic>>(
        future: getDataFuture,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: ((context, index) {
                  return ListTile(
                    onTap: () {
                      Get.toNamed('/detail', arguments: {
                        'id': snapshot.data!['data'][index]['id'],
                        'title': snapshot.data!['data'][index]['title'],
                      });
                    },
                    leading: ConstrainedBox(
                      constraints: const BoxConstraints(
                        maxHeight: 80,
                        maxWidth: 80,
                        minHeight: 80,
                        minWidth: 80,
                      ),
                      child: Image.network(
                        '${snapshot.data!['data'][index]['picture']}',
                        fit: BoxFit.fill,
                      ),
                    ),
                    title: Text('${snapshot.data!['data'][index]['title']}'),
                    subtitle:
                        Text('${snapshot.data!['data'][index]['detail']}'),
                    trailing: Chip(
                        label:
                            Text('${snapshot.data!['data'][index]['view']}')),
                  );
                }),
                separatorBuilder: (context, index) => const Divider(),
                itemCount: snapshot.data!['data'].length);
          } else if (snapshot.hasError) {
            return Center(
                child: Text('${snapshot.error}',
                    style: const TextStyle(color: Colors.red)));
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
