import 'package:baontapp/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({super.key});

  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MenuDrawer(),
      appBar: AppBar(title: const Text('about us')),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset('assets/images/maxresdefault.jpg'),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Baowee',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueGrey),
                    ),
                    const Divider(),
                    const Text(
                        'Consumer prices account for a majority of overall inflation. Inflation is important to currency valuation because rising prices lead the central bank to raise interest rates out of respect for their inflation containment mandate'),
                    const Divider(),
                    Row(
                      children: [
                        const Expanded(
                            child: Icon(
                          Icons.sunny,
                          color: Colors.orange,
                        )),
                        Expanded(
                          flex: 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                'baowee@nt.ntplc.co.th',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.amberAccent),
                              ),
                              Text('601 Sila, Muang Khonkaen')
                            ],
                          ),
                        )
                      ],
                    ),
                    const Divider(),
                    Wrap(
                      spacing: 4,
                      children: List.generate(
                          20,
                          (index) => Chip(
                                label: Text('${index + 1}'),
                                avatar: const Icon(Icons.backpack),
                                backgroundColor: Colors.blue[100],
                              )),
                    ),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/white.jpg'),
                          radius: 30,
                        ),
                        const CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/white.jpg'),
                          radius: 35,
                        ),
                        const CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/white.jpg'),
                          radius: 40,
                        ),
                        SizedBox(
                          width: 80,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: const [
                                Icon(
                                  Icons.home,
                                  color: Colors.blueGrey,
                                ),
                                Icon(
                                  Icons.face,
                                  color: Colors.orange,
                                ),
                                Icon(
                                  Icons.logout,
                                  color: Colors.amberAccent,
                                ),
                              ]),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
