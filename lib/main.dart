import 'package:baontapp/pages/about_page.dart';
import 'package:baontapp/pages/detail_page.dart';
import 'package:baontapp/pages/home_page.dart';
import 'package:baontapp/pages/login_page.dart';
import 'package:baontapp/pages/news_page.dart';
import 'package:baontapp/pages/product_page.dart';
import 'package:baontapp/pages/website_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

String? token;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  token = prefs.getString('token');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'baontapp',
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          canvasColor: Colors.yellow.shade100,
          textTheme: const TextTheme(
              headline4: TextStyle(
                  color: Color.fromRGBO(84, 88, 89, 1.0), fontSize: 80),
              headline2: TextStyle(
                  color: Color.fromRGBO(84, 88, 89, 1.0), fontSize: 25))),
      // home: const MyHomePage(),
      // home: const AboutPage(),
      initialRoute: '/',
      getPages: [
        GetPage(
            name: '/',
            page: () => token == null ? const LoginPage() : const MyHomePage(),
            transition: Transition.noTransition),
        GetPage(
            name: '/home',
            page: () => const MyHomePage(),
            transition: Transition.noTransition),
        GetPage(
            name: '/about',
            page: () => const AboutPage(),
            transition: Transition.downToUp),
        GetPage(
            name: '/product',
            page: () => const ProductPage(),
            transition: Transition.fadeIn),
        GetPage(
            name: '/detail',
            page: () => const DetailPage(),
            transition: Transition.fade),
        GetPage(
            name: '/news',
            page: () => const NewsPage(),
            transition: Transition.fade),
        GetPage(
            name: '/website',
            page: () => const WebsitePage(),
            transition: Transition.fade),
        GetPage(
            name: '/login',
            page: () => const LoginPage(),
            transition: Transition.fade),
      ],
    );
  }
}
